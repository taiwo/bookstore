# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Orders', type: :system do
  include ActiveJob::TestHelper

  setup do
    @order = create(:order)
  end

  it 'should visit the index' do
    visit '/orders'
    expect(page).to have_css('h1', text: 'Orders')
  end

  it 'should destroy an order' do
    visit '/orders'
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end
    expect(page).to have_content('Order was successfully destroyed')
  end

  it 'should check routing number' do
    LineItem.delete_all
    Order.delete_all

    visit '/'
    click_on 'Add to cart', match: :first
    click_on 'Checkout'

    fill_in 'order_name', with: 'Taiwo Ayanleye'
    fill_in 'order_address', with: '16A Fola Jinadu Crescent, Gbagada, Lagos'
    fill_in 'order_email', with: 'taiwo@example.com'

    expect(page).not_to have_css('#order_routing_number')

    select 'Check', from: 'Pay type'

    expect(page).to have_css('#order_routing_number')

    fill_in 'Routing #', with: '123456'
    fill_in 'Account #', with: '987654'

    perform_enqueued_jobs do
      click_button 'Place order'
    end

    orders = Order.all
    expect(orders.size).to eq(1)

    order = orders.first

    expect(order.name).to eq('Taiwo Ayanleye')
    expect(order.address).to eq('16A Fola Jinadu Crescent, Gbagada, Lagos')
    expect(order.email).to eq('taiwo@example.com')
    expect(order.pay_type).to eq('Check')
    expect(order.line_items.size).to eq(1)

    mail = ActionMailer::Base.deliveries.last
    expect(mail[:to].value).to eq('taiwo@example.com')
    expect(mail[:from].value).to eq('Taiwo Ayanleye <taiwo@example.com>')
    expect(mail.subject).to eq('Your Bookshop Order Confirmation')
  end
end
