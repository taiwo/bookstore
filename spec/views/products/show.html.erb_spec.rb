# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'products/show', type: :view do
  before(:each) do
    @product = create(:product, title: 'The gods are not to be blamed')
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(@product.title)
    expect(rendered).to match(@product.description)
    expect(rendered).to match(@product.image_url)
    # expect(rendered).to match(/@product.price/)
  end
end
