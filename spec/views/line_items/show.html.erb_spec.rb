# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'line_items/show', type: :view do
  let(:line_item) { build(:line_item) }
  before(:each) do
    @line_item = assign(:line_item, LineItem.create!(
                                      product: line_item.product,
                                      cart: line_item.cart
                                    ))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(//)
    expect(rendered).to match(//)
  end
end
