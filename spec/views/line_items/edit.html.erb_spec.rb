# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'line_items/edit', type: :view do
  let(:line_item) { create(:line_item) }
  before(:each) do
    @line_item = assign(:line_item, LineItem.create!(
                                      product: line_item.product,
                                      cart: line_item.cart
                                    ))
  end

  it 'renders the edit line_item form' do
    render

    assert_select 'form[action=?][method=?]', line_item_path(@line_item), 'post' do
      assert_select 'input[name=?]', 'line_item[product_id]'

      assert_select 'input[name=?]', 'line_item[cart_id]'
    end
  end
end
