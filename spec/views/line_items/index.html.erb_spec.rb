# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'line_items/index', type: :view do
  let(:line_items) { build_list(:line_item, 2) }
  before(:each) do
    assign(:line_items, [
             LineItem.create!(
               product: line_items.first.product,
               cart: line_items.first.cart
             ),
             LineItem.create!(
               product: line_items.second.product,
               cart: line_items.second.cart
             )
           ])
  end

  it 'renders a list of line_items' do
    # render
    # assert_select 'tr>td', text: nil.to_s, count: 2
    # assert_select 'tr>td', text: nil.to_s, count: 2
  end
end
