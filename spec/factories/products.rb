# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    title { "#{Faker::Book.title} #{rand(1000)}" }
    description { Faker::Lorem.paragraph_by_chars(number: 256, supplemental: false) }
    image_url { 'bad-blood.jpg' }
    price { Faker::Number.decimal(l_digits: 4, r_digits: 2) }
  end
end
