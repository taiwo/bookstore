FactoryBot.define do
  factory :user do
    name { Faker::Internet.username }
    password_digest { BCrypt::Password.create('secret') }
  end
end
