# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Product, type: :model do
  before(:all) do
    @product = create(:product)
    @products = create_list(:product, 2)
  end

  it 'should have valid attributes' do
    expect(@product).to be_valid
  end

  it 'should have valid image url' do
    expect(@product).to be_valid
  end

  it 'should not be valid with wrong url format' do
    @product.image_url = 'bad-blood.jpg.txt'
    expect(@product).to_not be_valid
  end

  it 'should not be valid without a unique title' do
    invalid_product = build(:product, title: @products.first.title)
    expect(invalid_product).to_not be_valid
  end
end
