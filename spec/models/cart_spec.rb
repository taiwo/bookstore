# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Cart, type: :model do
  before(:all) do
    @cart = create(:cart)
  end

  it 'should have valid attributes' do
    expect(@cart).to be_valid
  end
end
