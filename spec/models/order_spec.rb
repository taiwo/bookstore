# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Order, type: :model do
  before(:all) do
    @order = create(:order)
  end

  it 'should have valid attributes' do
    expect(@order).to be_valid
  end

  it 'should not be valid without a name' do
    @order.name = nil
    expect(@order).to_not be_valid
  end

  it 'should not be valid without an address' do
    @order.address = nil
    expect(@order).to_not be_valid
  end

  it 'should not be valid without an email' do
    @order.email = nil
    expect(@order).to_not be_valid
  end

  it 'should not be valid without a pay type' do
    @order.pay_type = nil
    expect(@order).to_not be_valid
  end
end
