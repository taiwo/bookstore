# frozen_string_literal: true

require 'rails_helper'

RSpec.describe LineItem, type: :model do
  before(:all) do
    @product = create(:product)
    @line_item = create(:line_item, product_id: @product.id)
  end

  it 'should have valid attributes' do
    expect(@line_item).to be_valid
  end

  it 'should not be valid without a product' do
    @line_item.product = nil
    expect(@line_item).to_not be_valid
  end
end
