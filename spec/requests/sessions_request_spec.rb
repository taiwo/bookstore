require 'rails_helper'

RSpec.describe 'Sessions', type: :request do
  let(:user) { create(:user) }
  describe 'GET /new' do
    it 'should prompt for login' do
      get login_url
      expect(response).to have_http_status(:success)
    end
  end

  describe 'GET /create' do
    it 'should login' do
      post login_url, params: { name: user.name, password: 'secret' }
      expect(response).to redirect_to(admin_url)
    end
  end

  describe 'GET /destroy' do
    it 'should logout' do
      delete logout_url
      expect(response).to redirect_to(store_index_url)
    end
  end
end
