# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Stores', type: :request do
  let(:products) { create_list(:product, 10) }
  describe 'GET /index' do
    it 'returns http success' do
      get store_index_url
      expect(response).to have_http_status(:success)
      expect(response).to render_template(:index)
      assert_select '.column', minimum: 5
      assert_select '.menu-list li', 4
      assert_select '.price', /₦[,\d]+\.\d\d/
    end
  end
end
