# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrderMailer, type: :mailer do
  describe 'received' do
    let(:order) { create(:order) }
    let(:product) { create(:product) }
    let(:line_item) { create(:line_item, product: product, order: order) }
    let(:mail) { OrderMailer.received(order) }

    it 'renders the headers' do
      expect(mail.subject).to eq('Your Bookshop Order Confirmation')
      expect(mail.to).to eq([order.email])
      expect(mail.from).to eq(['taiwo@example.com'])
    end

    # it 'renders the body' do
    #   expect(mail.body.encoded).to match(line_item.product.title)
    # end
  end

  describe 'shipped' do
    let(:order) { create(:order) }
    let(:product) { create(:product) }
    let(:line_item) { create(:line_item, product_id: product.id, order_id: order.id) }
    let(:mail) { OrderMailer.shipped(order) }

    it 'renders the headers' do
      expect(mail.subject).to eq('Your Bookshop Order Shipped')
      expect(mail.to).to eq([order.email])
      expect(mail.from).to eq(['taiwo@example.com'])
    end

    # it 'renders the body' do
    #   expect(mail.body.encoded).to match(line_item.product.title)
    # end
  end
end
