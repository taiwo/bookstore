# frozen_string_literal: true

puts 'Clearing product table.'
Product.delete_all
# . . .
puts 'Seeding new products.'
FactoryBot.create_list(:product, 10)
puts 'Done.'
