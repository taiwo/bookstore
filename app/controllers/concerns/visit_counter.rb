# frozen_string_literal: true

module VisitCounter
  private

  def set_counter
    session[:counter].nil? ? session[:counter] = 0 : session[:counter] += 1
    @counter = session[:counter]
  end

  def reset_counter
    session[:counter] = 0
  end
end
