# frozen_string_literal: true

# Store controller
class StoreController < ApplicationController
  skip_before_action :authorize

  include VisitCounter
  include CurrentCart
  before_action :set_counter
  before_action :set_cart
  def index
    if params[:set_locale]
      redirect_to store_index_url(locale)
    end
    @products = Product.order(:title)
  end
end
