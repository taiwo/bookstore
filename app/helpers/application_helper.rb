# frozen_string_literal: true

# Application helper
module ApplicationHelper
  def render_if(condition, record)
    render record if condition
  end
end
